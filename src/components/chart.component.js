import React, {useState, useEffect} from 'react';

import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);


const options = {
  responsive: true,
  plugins: {
    legend: {
      position: 'top',
    },
    title: {
      display: true,
      text: 'Chart.js Line Chart',
    },
  },
};

const arrFunc = (dataArr, boolFix, axis=true) => {
  let arr = []

  for (var i = 1; i <= dataArr.length; i++) {
    if (axis) {
      arr.push(i);
    } else {
      arr.push(dataArr[i]);
    }
  };

  if (boolFix) {
    arr = arr.slice(-100)
  };

  if (axis) {
    return arr.map((e) => e.toString())
  } else {
    return arr
  }
  
};


const ChartComponent = ({dataTS}) => {
    const [fixX, setFixX] = useState(false);
    const [labels, setLabels] = useState(() => {
      const arr = arrFunc(dataTS[0], fixX)
      return arr
    });
    const [dataPlot, setDataPlot] = useState(() => {
      const arr = dataTS.map((e) => {
        return arrFunc(e, fixX, false)
      });
      return arr
    });

    useEffect(() => {
      setLabels(() => {
        const arr = arrFunc(dataTS[0], fixX)
        return arr
      });
      
      setDataPlot(() => {
        const arr = dataTS.map((e) => {
          return arrFunc(e, fixX, false)
        });
        return arr
      });
    }, [dataTS, fixX]);

    const data = {
      labels,
      datasets: [
        {
          label: 'TimeSerie A',
          data: dataPlot[0],
          borderColor: 'rgb(255, 50, 50)',
          backgroundColor: 'rgba(255, 100, 100, 0.5)',
        },
        {
          label: 'TimeSerie B',
          data: dataPlot[1],
          borderColor: 'rgb(50, 255, 50)',
          backgroundColor: 'rgba(100, 255, 100, 0.5)',
        },
        {
          label: 'TimeSerie C',
          data: dataPlot[2],
          borderColor: 'rgba(50, 50, 255)',
          backgroundColor: 'rgba(100, 100, 255, 0.5)',
        },
        {
          type: "bubble",
          label: "BUBS",
          backgroundColor: "rgba(0, 255, 0, 0.4)",
          data: [
              { x: "M", y: 0, r: 20 }
          ]
      }
      ]
    }
    
    const handleXDisplay = () => {
      setFixX(!fixX);
    };

    return (
      <div>
        <button onClick={handleXDisplay}>Fix numb of Xs (100)</button>
        <Line options={options} data={data} />
      </div>
    )
}

export default ChartComponent;