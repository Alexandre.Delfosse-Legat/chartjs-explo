import React, { useRef } from 'react';

import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import ZoomOutIcon from '@mui/icons-material/ZoomOut';
import SaveIcon from '@mui/icons-material/Save';

import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    ScatterController,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';

import { Chart } from 'react-chartjs-2';
import zoomPlugin from 'chartjs-plugin-zoom';

ChartJS.register(
    CategoryScale,
    LinearScale,
    ScatterController,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
    zoomPlugin
);

const options = {
    responsive: true,
    plugins: {
        legend: {
            display: false
        },
        title: {
            display: false
        },
        zoom: {
            pan: {
                enabled: true,
                mode: 'xy',
                modifierKey: 'ctrl',
            },
            zoom: {
                mode: 'xy',
                drag: {
                    enabled: true,
                    borderColor: 'rgb(54, 162, 235)',
                    borderWidth: 1,
                    backgroundColor: 'rgba(54, 162, 235, 0.3)'
                }
            }
        },
        tooltip: {
            callbacks: {
                title: function () { },
                label: (context) => {
                    return context.raw.text
                }
            }
        }
    }
};

function keysrt(key) {
    return function (a, b) {
        if (Number(a[key]) > Number(b[key])) return 1;
        if (Number(a[key]) < Number(b[key])) return -1;
        return 0;
    }
}

function downloadFunc(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

const lastTradeString = (arr) => {
    let strOut = 'Last trade @' + String(arr[0]).slice(-8) + ': ' + String(arr[1]) + ' ' + (arr[2] === 1 ? 'call x' : 'put x') + String(arr[5])
    strOut += ', iVol: ' + String(arr[3]) + ', price: ' + String(arr[4])
    return strOut
};

const ChartBubbleComponentBis = ({symbol, volCurve, tradeTicks}) => {
    const chartRef = useRef(null);

    const volData = Object.keys(volCurve).map((e) => {
        return {
            x: e,
            y: volCurve[e][1] * 100,
            text: [
                'strike: ' + String(Number(e)),
                'delta (100): ' + String(Math.round(Math.min(Math.abs(volCurve[e][0]), Math.abs(volCurve[e][0] - 1)) * 100)),
                'ivol: ' + String(Math.round(volCurve[e][1] * 1000) / 10)
            ]
        }
    }).sort(keysrt('x'))

    const tradeDataArrayFiltered = Object.entries(tradeTicks).filter(([key, value]) => {
        if (value[6] !== 73) {
            return false
        } else {
            return true
        }
    });

    const tradeDataDisplay = tradeDataArrayFiltered.map(([key, value]) => {
        return {
            x: value[1],
            y: value[3],
            r: Math.min(15, 5 + value[5] / 1),
            text: [
                'strike: ' + value[1],
                'iVol: ' + String(Math.round(value[3] * 100) / 100),
                'trade size: ' + String(value[5]),
                'trade time: ' + String(value[0])
            ],
        }
    });

    const tradeDataBackground = tradeDataArrayFiltered.map(([key, value]) => {
        const tradeTime = new Date(value[0])
        const nowTime = new Date()
        const gVal = 0 + ((nowTime - tradeTime) / 91800)
        const alphaVal = 1 - ((nowTime - tradeTime) / 23400000)
        return 'rgba('+ String(gVal) + ', ' + String(gVal) + ', '+ String(gVal) + ', ' + String(alphaVal) + ')'
    })


    const data = {
        datasets: [
            {
                type: 'scatter',
                label: 'iVolCurve',
                showLine: true,
                pointRadius: 2,
                data: volData,
                borderColor: 'rgba(255, 0, 0, 1)',
                backgroundColor: 'rgba(255, 0, 0, 1)',
            },
            {
                type: "bubble",
                label: 'Trades',
                backgroundColor: tradeDataBackground,
                borderColor: tradeDataBackground,
                data: tradeDataDisplay,
            }
        ]
    }

    const handleResetZoom = () => {
        chartRef.current.resetZoom();
    };

    const handleDownload = () => {
        console.log('getZoomLevel: ', chartRef.current.getZoomLevel())
        // downloadFunc(String(symbol) + '.json', JSON.stringify(Object.fromEntries(tradeDataArrayFiltered)));
    };

    return (
        <Container style={{
            display: 'flex',
            flexDirection: 'column',
            flexWrap: 'wrap',
            justifyContent: 'center',
            alignItems: 'center',
            alignContent: 'center',
            padding: '1em 0',
            border: '2px solid black',
            borderRadius: '10px'
        }}>
            <Container style={{
                width: '100%',
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'space-between',
                alignItems: 'center',
                alignContent: 'center',
            }}>

                <Container style={{
                    width: '50%',
                    display: 'flex',
                    flexWrap: 'wrap',
                    justifyContent: 'start',
                    alignItems: 'center',
                    alignContent: 'center',
                }}>
                    <Typography variant="h6">
                        {symbol}
                    </Typography>
                </Container>

                <Container style={{
                    width: '50%',
                    display: 'flex',
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    justifyContent: 'end',
                    alignItems: 'center',
                    alignContent: 'center',
                }}>
                    <IconButton onClick={handleResetZoom} color="primary" aria-label="reset-zoom">
                        <ZoomOutIcon/>
                    </IconButton>
                    <IconButton onClick={handleDownload} color="primary" aria-label="download-data">
                        <SaveIcon/>
                    </IconButton>
                </Container>
            </Container>

            <Container style={{margin: '0'}}>
                <Typography variant="body">
                    {lastTradeString(tradeDataArrayFiltered[tradeDataArrayFiltered.length - 1][1])}
                </Typography>
            </Container>

            <Container style={{margin: '0'}}>
                <Chart ref={chartRef} options={options} data={data} />
            </Container>
        </Container>
    )
}

export default ChartBubbleComponentBis;