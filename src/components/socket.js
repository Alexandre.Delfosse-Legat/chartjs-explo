import { io } from "socket.io-client";

if (!window.socket) {
  // window.socket = io.connect("http://ws85-ams:5000");
  const protocol = window.location.protocol; //http: or https:
  window.socket = io.connect(`${protocol}//${window.location.hostname}`);
  console.log(protocol, window.location.hostname)
}
export default window.socket;
