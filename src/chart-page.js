import tradeTicks from './data/trade_ticks';
import volCurve from './data/vol_curve';

import React, { useState, useEffect } from 'react';

import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';

import ChartBubbleComponent from './components/chart_bubble';
import ChartBubbleComponentBis from './components/chart_bubbles_2';

const createOptions = () => {
    return {
        responsive: true,
        plugins: {
            legend: {
                display: false
            },
            title: {
                display: false
            },
            zoom: {
                pan: {
                    enabled: true,
                    mode: 'xy',
                    modifierKey: 'ctrl',
                },
                zoom: {
                    mode: 'xy',
                    drag: {
                        enabled: true,
                        borderColor: 'rgb(54, 162, 235)',
                        borderWidth: 1,
                        backgroundColor: 'rgba(54, 162, 235, 0.3)'
                    }
                }
            },
            tooltip: {
                callbacks: {
                    title: function () { },
                    label: (context) => {
                        return context.raw.text
                    }
                }
            }
        }
    }
}

const ChartPage = (props) => {

    const [data, setData] = useState(() => {
        const arr = []
        arr.push(['SPY', volCurve, tradeTicks])
        arr.push(['SPY', volCurve, tradeTicks])
        return arr
    })

    const optionsArr = data.map((e) => {
        const chartOptions = createOptions(); 
        return chartOptions
    })

    const [counta, setCounta] = useState(1)
    useEffect(() => {
        const timerrr = setTimeout(() => {
            setData(() => {
                //load trades incrementally on left chart
                const arrTradesToShow = Object.entries(tradeTicks).slice(0, counta * 5)
                setCounta(counta + 1)
                

                let objTradesToShow = {}
                for (let i = 0; i < arrTradesToShow.length; i++) {
                    objTradesToShow[arrTradesToShow[i][0]] = arrTradesToShow[i][1]
                };


                const newTrades = [data[0][0], data[0][1], objTradesToShow]

                //change volcurve on right chart
                const oldVolCurve = data[1][1]

                let count = 0
                const newVolCurveArr = Object.entries(oldVolCurve).map((e) => {
                    const oldVolData = e[1];
                    const newVolData = [oldVolData[0], oldVolData[1] + count/10000]
                    count += 1
                    return [e[0], newVolData]
                })

                const newVolCurveObj = {}
                for (let i = 0; i < newVolCurveArr.length; i++) {
                    newVolCurveObj[newVolCurveArr[i][0]] = newVolCurveArr[i][1]
                };

                const newVol = [data[1][0], newVolCurveObj, data[1][2]]
                
                return [newTrades, newVol]
            });
        }, 1000);
    });

    return (
        <Box sx={{ flexGrow: 1 }}>
            <Grid container spacing={1}>

                <Grid key={1} item xs={6}>
                    <ChartBubbleComponent symbol={data[0][0]} volCurve={data[0][1]} tradeTicks={data[0][2]} />
                </Grid>

                <Grid key={2} item xs={6}>
                    <ChartBubbleComponent symbol={data[1][0]} volCurve={data[1][1]} tradeTicks={data[1][2]} />
                </Grid>

                <Grid key={3} item xs={6}>
                    <ChartBubbleComponent symbol={data[0][0]} volCurve={data[0][1]} tradeTicks={data[0][2]} />
                </Grid>

                <Grid key={4} item xs={6}>
                    <ChartBubbleComponent symbol={data[1][0]} volCurve={data[1][1]} tradeTicks={data[1][2]} />
                </Grid>

            </Grid>
        </Box>
    )
}

export default ChartPage;