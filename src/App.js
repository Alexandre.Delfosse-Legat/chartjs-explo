import React, { useEffect, useState } from 'react';
import socket from './socket';
import ChartPage from './chart-page';

// import tradeTicks from './data/trade_ticks';
// import volCurve from './data/vol_curve';

function App() {
  const [socketConnected, setSocketConnected] = useState(false);
  const [t, setT] = useState('');
  const [dataPlot, setDataPlot] = useState([0]);
  const [dataPlotB, setDataPlotB] = useState([0]);
  const [dataPlotC, setDataPlotC] = useState([0]);

  // console.log('volCurve: ', volCurve);

  // subscribe to the socket event
  useEffect(() => {
    if (!socket) return;

    socket.on('connect', () => {
      console.log('connect fe 1')
      setSocketConnected(socket.connected);
      console.log('connect fe 2')
      subscribeToDateEvent();
      console.log('connect fe 3')
    });
    socket.on('disconnect', () => {
      setSocketConnected(socket.connected);
    });

    socket.on("getTime", data => {
      setT(data);
      setDataPlot(dataPlot => [...dataPlot, Number(dataPlot.slice(-1)) + Math.random()*10]);
      setDataPlotB(dataPlotB => [...dataPlotB, Number(dataPlotB.slice(-1)) + Math.random()*20]);
      setDataPlotC(dataPlotC => [...dataPlotC, Number(dataPlotC.slice(-1)) + Math.random()*30]);
    });

  }, [socket]);

  // manage socket connection
  const handleSocketConnection = () => {
    console.log(socketConnected)
    if (socketConnected)
      socket.disconnect();
    else {
      socket.connect();
    }
  }

  // subscribe to socket date event
  const subscribeToDateEvent = (interval = 1000) => {
    socket.emit('subscribeToDateEvent', interval);
  }

  const handleToggleDateFormat = () => {
    socket.emit('toggleDateFormat');
  };


  return (
    <div>
      <h2>Hello!</h2>

      <div><b>Connection status:</b> {socketConnected ? 'Connected' : 'Disconnected'}</div>
      <ChartPage />
    </div>
  );
}

export default App;