import { io } from "socket.io-client";

if (!window.socket) {
  // window.socket = io.connect("http://ws76-ams:5000");
  const protocol = window.location.protocol; //http: or https:
  const conn = `${protocol}//${window.location.hostname}:4000`
  window.socket = io(conn);
  console.log('conn: ', conn)
}
export default window.socket;